<?php

namespace Drupal\oembed_provider;

use Embed\Embed;
use Embed\Exceptions\EmbedException;
use Embed\Exceptions\InvalidUrlException;

use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Utility\SafeMarkup;

/**
 * A service class for building oEmbed output.
 */
interface OembedProviderInterface {

  public function __construct(array $config = []);

  public function getConfig();

  public function setConfig(array $config);

  /**
   * Retrieve the oEmbed code from a specified request.
   *
   * @param string|\Embed\Request $request
   *   The url or a request with the url
   * @param array $config
   *   (optional) Options passed to the adapter. If not provided the default
   *   options on the service will be used.
   *
   * @return []
   *   The oEmbed data.
   */
  public function getOembed($request, array $config = []);

}
