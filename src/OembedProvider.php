<?php

namespace Drupal\oembed_provider;

use Embed\Embed;
use Embed\Exceptions\EmbedException;
use Embed\Exceptions\InvalidUrlException;

use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Utility\SafeMarkup;

class OembedProvider {

  use StringTranslationTrait;

  /**
   * @var array
   */
  public $config;

  /**
   * @{inheritdoc}
   */
  public function __construct(array $config = []) {
    $this->config = $config;
  }

  /**
   * @{inheritdoc}
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * @{inheritdoc}
   */
  public function setConfig(array $config) {
    $this->config = $config;
  }

  /**
   * @{inheritdoc}
   */
  public function getOembed($request, array $config = []) {
    $embed_data = [];

    try {
      $embed = Embed::create($request, $config ?: $this->config);
      $embed_providers = $embed->getProviders();
      $embed_data = $embed_providers['oembed']->getBag()->getAll();
    }
    catch (InvalidUrlException $e) {
      $response = $e->getResponse();

      $embed_data = [
        'status' => $response->getStatusCode(),
        'error' =>  (string) $this->t('The destination URL is no longer available and provider replied with 4xx or 5xx error, or there was a problem establishing a connection to the server.'),
        'url' => (string) $response->getStartingUrl()
      ];
    }
    catch (EmbedException $e) {
      watchdog_exception('oembed_provider', $e);
    }

    return $embed_data;
  }

}
