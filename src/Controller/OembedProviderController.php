<?php

namespace Drupal\oembed_provider\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\oembed_provider\OembedProvider;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class OembedProviderController.
 */
class OembedProviderController extends ControllerBase {

  /**
   * Serve oEmbed end point.
   *
   * @return array
   *   The render array representing the oembed contnet.
   */
  public function serve() {
    $url  = \Drupal::request()->query->get('url');
    $configuration = \Drupal::request()->query->all();

    $oembed_provider = new OembedProvider();
    $embed_data = $oembed_provider->getOembed($url, $configuration);

    $response = new JsonResponse($embed_data);

    if (!empty($configuration['callback'])) {
      $response->setCallback($configuration['callback']);
    }

    return $response;
  }

}
